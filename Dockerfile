FROM node:19.7.0-alpine
ENV NODE_ENV=staging

RUN mkdir -p /usr/app
WORKDIR /usr/app

COPY ./ ./

RUN npm install
# RUN npm run build

EXPOSE 3000
CMD ["npm", "start"]