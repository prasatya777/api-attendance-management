import { DataSource } from "typeorm";
require('dotenv').config();

const attendanceDb =  new DataSource({
    type               : "mysql",
    name               : "attendanceManagementDb",
    host               : process.env.DB_HOST,
    port               : Number(process.env.DB_PORT),
    username           : process.env.DB_USERNAME,
    password           : process.env.DB_PASSWORD,
    database           : process.env.DB_DATABASE,
    entities           : [__dirname+'/src/entity/mysql/attendanceDb/*.entity.{ts,js}'],
    migrations         : [__dirname+'/src/migration/mysql/attendanceDb/*.{ts,js}'],
    synchronize        : false,
    logging            : false,
})

const mongoDb =  new DataSource({
    type              : "mongodb",
    name              : "mongodbLiveChatTicket",
    host              : process.env.DB_MONGO_HOST,
    port              : Number(process.env.DB_MONGO_PORT),
    username          : process.env.DB_MONGO_USERNAME,
    password          : process.env.DB_MONGO_PASSWORD,
    database          : process.env.DB_MONGO_DATABASE,
    entities          : [__dirname+'/src/entity/mongodb/liveChatTicket/*.entity.ts'],
    synchronize       : false,
    logging           : false,
    useNewUrlParser   : true,
    useUnifiedTopology: true
})

export{mongoDb, attendanceDb}