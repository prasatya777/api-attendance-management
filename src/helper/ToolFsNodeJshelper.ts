import fs = require('fs')
import ApiHelper from './ApiHelper';
import getCurrentLine from 'get-current-line';
import { DefaultTimeZone } from '../constant/DefaultConstant';

import moment           = require('moment');
import momentTz         = require('moment-timezone');
const  LogWinstonHelper = require('./LogWinstonHelper')

class ToolFsNodeJshelper{
    static deleteFile = async(path, fileName:any) =>{
        try {
            await Promise.all(fileName.map(async(v)=>{
                await fs.unlinkSync(path+v);
            }))
            
        } catch (error) {

            LogWinstonHelper.loggerWinstonGlobal.error({
                collectData: {
                    date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                    location: getCurrentLine(),
                    reqData : {
                        path: path,
                        fileName: fileName,
                    }
                },
                message : error.stack + '\n'
            })

            return ApiHelper.builFuncResponse(false, 'Delete File Failed');
        }

        return ApiHelper.builFuncResponse(true, 'Delete File Success');
    }

    static deleteFileCron = async(path, fileName:any, collectFileExisting:any) =>{
        try {
            let collectDelete = []
            await Promise.all(collectFileExisting.map(async(v)=>{
                if(!fileName.includes(v)){
                    if(fs.existsSync(path+v)){
                        collectDelete.push(path+v)
                        await fs.unlinkSync(path+v);
                    }
                }
            }))

            LogWinstonHelper.loggerCron.info({
                collectData: {
                    date: momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                    task: 'imageDeleteInboundPackageNotSaveDb',
                    type: 'DELETE',
                    daya: collectDelete
                },
                message : 'List Cron Image Delete Inbound Package Not Save Db'
            })
            
        } catch (error) {

            LogWinstonHelper.loggerCron.error({
                collectData: {
                    date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                    location: getCurrentLine(),
                    task: 'imageDeleteInboundPackageNotSaveDb',
                    reqData : {
                        path: path,
                        fileName: fileName,
                    }
                },
                message : error.stack + '\n'
            })

            return ApiHelper.builFuncResponse(false, 'Delete File Failed');
        }

        return ApiHelper.builFuncResponse(true, 'Delete File Success');
    }
}

export default ToolFsNodeJshelper