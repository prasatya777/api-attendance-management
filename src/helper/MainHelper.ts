import ApiHelper from './ApiHelper';
import moment = require('moment');
import momentTz = require('moment-timezone');
import getCurrentLine from 'get-current-line';
import { DefaultTimeZone } from '../constant/DefaultConstant';
const LogWinstonHelper = require('./LogWinstonHelper')

class MainHelper{

    static validateJSON = async(json) =>{
        try {
            const resParse = JSON.parse(json);
            if(!resParse.length){
                return ApiHelper.builFuncResponse(false, 'Not Format JSON');
            }

            return ApiHelper.builFuncResponse(true, 'Format JSON', resParse);
            
        } catch (error) {

            LogWinstonHelper.loggerWinstonGlobal.error({
                collectData: {
                    date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                    location: getCurrentLine(),
                    reqData : {
                        json : json,
                    }
                },
                message : error.stack + '\n'
            })

            return ApiHelper.builFuncResponse(false, 'Not Format JSON');
        }
    }

    static validateArray = async(arrayCollect) =>{
        try {
            if( Array.isArray(arrayCollect)){
                return ApiHelper.builFuncResponse(true, 'Format Array', arrayCollect);
            }
            return ApiHelper.builFuncResponse(false, 'Not Format Array');
        } catch (error) {

            LogWinstonHelper.loggerWinstonGlobal.error({
                collectData: {
                    date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                    location: getCurrentLine(),
                    reqData : {
                        arrayCollect : arrayCollect,
                    }
                },
                message : error.stack + '\n'
            })

            return ApiHelper.builFuncResponse(false, 'Not Format Array');
        }
    }

    // REVIEW Generate Random Alpha, Numeric, and Symbol
        static generateRandomCharacter = async(type='numeric-caps-alpha', length=4) => {
            try {
                // REVIEW Define Random
                    var alphabet       = '';
                    var generateRandom = '';

                    if(type == 'alpha') {
                        alphabet = 'abcdefghijklmnopqrstuvwxyz';
                    } else if(type == 'alpha-caps') {
                        alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    } else if(type == 'alpha-numeric') {
                        alphabet = 'abcdefghijklmnopqrstuvwxyz1234567890';
                    } else if(type == 'alpha-numeric-caps') {
                        alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
                    } else if(type == 'alpha-numeric-symbols') {
                        alphabet = 'abcdefghijklmnopqrstuvwxyz1234567890~!@#$%^&*()_+-=';
                    } else if(type == 'alpha-numeric-caps-symbols') {
                        alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890~!@#$%^&*()_+-=';
                    } else if(type == 'numeric-caps-alpha') {
                        alphabet = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                    } else if(type == 'numeric-caps') {
                        alphabet = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    } else if(type == 'numeric') {
                        alphabet = '1234567890';
                    }
                
                var alphabetLength = alphabet.length - 1;

                for (var i = 0; i < length; i++) {
                    var randomNumber = Math.floor(Math.random() * alphabetLength) + 1;
                    generateRandom += alphabet[randomNumber];
                }

                return ApiHelper.builFuncResponse(true, 'Generate Random Success', {generateRandom: generateRandom});
                
            } catch (error) {

                LogWinstonHelper.loggerWinstonGlobal.error({
                    collectData: {
                        date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                        location: getCurrentLine(),
                        reqData : {
                            type  : type,
                            length: length,
                        }
                    },
                    message : error.stack + '\n'
                })

                return ApiHelper.builFuncResponse(false, 'Generate Random Failed');
            }
        }

    // REVIEW Get Detail Range Date
        static getDetailDateRange = async(startDate, endDate) : Promise<any> => {
            try {
                const startDateRange = moment(startDate,'YYYY-MM-DD')
                const endDateRange = moment(endDate,'YYYY-MM-DD')
                
                // NOTE Set Range Month
                    let tempStartDate = startDateRange.clone()
                    let tempEndDate = endDateRange.clone()
                
                    const funcCollectMonthDay = (tempStartDate, tempEndDate) => {
                        let resCollectMonthDay = []
                        let tempCollectMonth = []
                                
                        while (tempStartDate.isSameOrBefore(tempEndDate)) {
                            tempCollectMonth.push(tempStartDate.clone().format('YYYY-MM-DD'))
                            if(tempStartDate.isSame(moment(tempStartDate.clone().endOf('month').format('YYYY-MM-DD'), 'YYYY-MM-DD'))){
                            resCollectMonthDay.push({
                                monthName: tempStartDate.clone().format('MMM')+" "+tempStartDate.clone().format('YY'),
                                detail   : tempCollectMonth[0]+' - '+tempCollectMonth[tempCollectMonth.length - 1],
                                data     : tempCollectMonth
                            })
                            tempCollectMonth = []
                            }else if(tempStartDate.isSame(tempEndDate)){
                                resCollectMonthDay.push({
                                    monthName: tempStartDate.clone().format('MMM')+" "+tempStartDate.clone().format('YY'),
                                    detail   : tempCollectMonth[0]+' - '+tempCollectMonth[tempCollectMonth.length - 1],
                                    data     : tempCollectMonth
                                })
                                tempCollectMonth = []
                            }
                            
                            tempStartDate.add(1, 'days')
                        }
                        
                        return resCollectMonthDay
                    }
        
                    const collectMonthDay = await funcCollectMonthDay(tempStartDate, tempEndDate)

                // NOTE Set Range Week
                    tempStartDate = startDateRange.clone()
                    tempEndDate = endDateRange.clone()
                
                    const funcCollectWeekDay = (tempStartDate, tempEndDate) => {
                        let resCollectWeekDay = []
                        let tempCollectWeek   = []
                        let startLoopWeek     = true
                        let countLoop         = 0

                        while(startLoopWeek) {
                            tempCollectWeek = []
                                while(tempStartDate.format('dddd') != 'Sunday' && tempStartDate.isSameOrBefore(tempEndDate)){
                                    tempCollectWeek.push(tempStartDate.clone().format('YYYY-MM-DD'))
                                    tempStartDate.add(1, 'days')
                                }

                                if(tempStartDate.format('dddd') == 'Sunday' && tempStartDate.isSameOrBefore(tempEndDate)){
                                tempCollectWeek.push(tempStartDate.clone().format('YYYY-MM-DD'))
                                tempStartDate.add(1, 'days')
                                }

                                if(tempCollectWeek.length > 0){
                                resCollectWeekDay[Number(countLoop)] = {
                                    weekName: moment(tempCollectWeek[0], 'YYYY-MM-DD').isoWeek(),
                                    detail  : tempCollectWeek[0]+' - '+ tempCollectWeek[tempCollectWeek.length - 1],
                                    data    : tempCollectWeek
                                }
                                }
                                
                                if(!tempStartDate.isSameOrBefore(tempEndDate)){
                                startLoopWeek = false;
                            }

                            countLoop++
                            
                        }
                        return resCollectWeekDay
                    }

                    const collectWeekDay = await funcCollectWeekDay(tempStartDate, tempEndDate)
            
                // NOTE Set Range Date Day
                    tempStartDate = startDateRange.clone()
                    tempEndDate = endDateRange.clone()

                    const funcCollectDay = (tempStartDate, tempEndDate) => {
                        let tempCollectDay = []
                    while (tempStartDate.isSameOrBefore(tempEndDate)) {
                        tempCollectDay.push({
                            dayName: tempStartDate.clone().format('ddd'),
                            date    : tempStartDate.clone().format('YYYY-MM-DD')
                        })
                        tempStartDate.add(1, 'days')
                    }
                    return tempCollectDay
                    }

                    const collectDay = await funcCollectDay(tempStartDate, tempEndDate)
            
            
                // NOTE Set Range Date Group Day
                    tempStartDate = startDateRange.clone()
                    tempEndDate = endDateRange.clone()
                    
                    const funcCollectGroupDay = async(tempStartDate, tempEndDate) => {
                        let tempCollectDay      = []
                        while (tempStartDate.isSameOrBefore(tempEndDate)) {
                        tempCollectDay.push({
                            dayName: tempStartDate.clone().format('ddd'),
                            date    : tempStartDate.clone().format('YYYY-MM-DD')
                        })
                        tempStartDate.add(1, 'days')
                        }
            
                        const funcSubCollectGroupDay = (collectDay) => {
                            const uniqueDayNameCollect = Array.from(new Set(collectDay.map((val) => val.dayName)))
                            
                            const resGroupDay = Promise.all(uniqueDayNameCollect.map(async(valUnique) : Promise<any> => {
                                let tempFilterGroupDay = await Promise.all(
                                    collectDay.filter((valCollect)=> valCollect['dayName'] == valUnique)
                                )
                                
                                return {
                                    dayGroupName: valUnique.toString(),
                                    total       : tempFilterGroupDay.length > 0 ? tempFilterGroupDay.length: '-',
                                    data        : tempFilterGroupDay.length > 0 ? tempFilterGroupDay       : []
                                }
                            
                            }))

                            return resGroupDay
                        }
                    
                        return await funcSubCollectGroupDay(tempCollectDay)
                    }
            
                    const collectGroupDay = await funcCollectGroupDay(tempStartDate, tempEndDate)
            
                return ApiHelper.builFuncResponse(true, 'Get Detail Date Range Success', {
                    month   : collectMonthDay,
                    week    : collectWeekDay,
                    day     : collectDay,
                    groupDay: collectGroupDay
                });
            } catch (error) {
                return ApiHelper.builFuncResponse(false, error.message);
            }
        }

    // REVIEW Convert Data Detail User Id
        static reFormatPhoneNumberByCountry = async(phoneNumber, countryCode='ID') => {
            try {

            // REVIEW Replace Other Character Number
                const phoneNumberOnly = phoneNumber.replace(/[^0-9]/g,'')

            // REVIEW Set Default Phone
                let phoneNumberCountry  = phoneNumberOnly.substr(0,4).match(/62|60|63/i) ? phoneNumberOnly : null;
                let phoneNumberNational = phoneNumberOnly.substr(0,4).match(/62|60|63/i) ? null : ( !phoneNumberOnly.substr(0,1).match(/0/i) ? "0"+phoneNumberOnly : phoneNumberOnly );

                let parseCode           = null
                let parseCodeZero       = null
                switch (countryCode) {
                    case 'ID':
                        parseCode     = phoneNumberOnly.substr(0,4)
                        parseCodeZero = phoneNumberOnly.substr(0,1)
                        // console.log(parseCode, parseCodeZero, phoneNumberCountry, phoneNumberNational)
                        if(parseCode.match(/62/i) && !phoneNumberNational){
                            let parseMainPhone      = phoneNumberOnly.substr(2)
                                phoneNumberNational = "0"+parseMainPhone;
                        }else if(parseCodeZero.match(/0/i) && !phoneNumberCountry){
                            let parseMainPhone     = phoneNumberOnly.substr(1);
                                phoneNumberCountry = "62"+parseMainPhone;
                        }
                        else if(!parseCodeZero.match(/0/i) && !phoneNumberCountry){
                            phoneNumberCountry = "62"+phoneNumberOnly;
                        }
                    break;

                    case 'MY':
                        parseCode     = phoneNumberOnly.substr(0,4)
                        parseCodeZero = phoneNumberOnly.substr(0,1)

                        if(parseCode.match(/60/i) && !phoneNumberNational){
                            let parseMainPhone      = phoneNumberOnly.substr(2)
                                phoneNumberNational = "0"+parseMainPhone;
                        }else if(parseCodeZero.match(/0/i) && !phoneNumberCountry){
                            let parseMainPhone     = phoneNumberOnly.substr(1);
                                phoneNumberCountry = "60"+parseMainPhone;
                        }
                        else if(!parseCodeZero.match(/0/i) && !phoneNumberCountry){
                            phoneNumberCountry = "60"+phoneNumberOnly;
                        }
                    break;

                    case 'PH':
                        parseCode     = phoneNumberOnly.substr(0,4)
                        parseCodeZero = phoneNumberOnly.substr(0,1)

                        if(parseCode.match(/63/i) && !phoneNumberNational){
                            let parseMainPhone      = phoneNumberOnly.substr(2)
                                phoneNumberNational = "0"+parseMainPhone;
                        }else if(parseCodeZero.match(/0/i) && !phoneNumberCountry){
                            let parseMainPhone     = phoneNumberOnly.substr(1);
                                phoneNumberCountry = "63"+parseMainPhone;
                        }
                        else if(!parseCodeZero.match(/0/i) && !phoneNumberCountry){
                            phoneNumberCountry = "63"+phoneNumberOnly;
                        }
                    break;
                
                    default:
                        parseCode     = phoneNumberOnly.substr(0,4)
                        parseCodeZero = phoneNumberOnly.substr(0,1)

                        if(parseCode.match(/62/i) && !phoneNumberNational){
                            let parseMainPhone      = phoneNumberOnly.substr(2)
                                phoneNumberNational = "0"+parseMainPhone;
                        }else if(parseCodeZero.match(/0/i) && !phoneNumberCountry){
                            let parseMainPhone     = phoneNumberOnly.substr(1);
                                phoneNumberCountry = "62"+parseMainPhone;
                        }
                        else if(!parseCodeZero.match(/0/i) && !phoneNumberCountry){
                            phoneNumberCountry = "62"+phoneNumberOnly;
                        }
                    break;
                }

                const resFinal = {
                    phoneNational:{
                        phone: phoneNumberNational,
                    },
                    phoneCountryCode:{
                        phone: phoneNumberCountry,
                    },
                }

                return ApiHelper.builFuncResponse(true, 'Convert Success', resFinal);
                
            } catch (error) {

                LogWinstonHelper.loggerWinstonGlobal.error({
                    collectData: {
                        date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                        location: getCurrentLine(),
                        reqData : {
                            phoneNumber: phoneNumber,
                            countryCode: countryCode,
                        }
                    },
                    message : error.stack + '\n'
                })

                return ApiHelper.builFuncResponse(false, 'Convert Failed', []);
            }
        }

    // REVIEW Get Country Code From Phone Number
        static getCountryCodeFromPhoneNumber = async(phoneNumber) => {
            try {

            // REVIEW Replace Other Character Number
                const phoneNumberOnly = phoneNumber.replace('/[^0-9]/g','')
                let   phoneCode       = phoneNumberOnly.substr(0,1)

                if( phoneCode == "6" ) {
                    phoneCode = phoneNumberOnly.substr(0,2);
                }else if( phoneCode != "6" && phoneCode != "0") {
                    phoneCode = "0"+phoneCode;
                }else if( phoneCode == "0") {
                    phoneCode = phoneNumberOnly.substr(0,2);
                }

                if ( phoneCode == "62" || phoneCode == "08" ) {
                    return ApiHelper.builFuncResponse(true, 'Get Data Success', {
                        country         : "ID",
                        phoneCountryCode: "62",
                        phoneNational   : "08",
                    })
                } else if ( phoneCode == "60" || phoneCode == "01") {
                    return ApiHelper.builFuncResponse(true, 'Get Data Success', {
                        country         : "MY",
                        phoneCountryCode: "60",
                        phoneNational   : "01",
                    })
                } else if ( phoneCode == "63" || phoneCode == "09"){
                    return ApiHelper.builFuncResponse(true, 'Get Data Success', {
                        country         : "PH",
                        phoneCountryCode: "63",
                        phoneNational   : "09",
                    })
                }
                else {
                    return ApiHelper.builFuncResponse(true, 'Get Data Success', {
                        country         : "ID",
                        phoneCountryCode: "62",
                        phoneNational   : "08",
                    })
                }
                
            } catch (error) {

                LogWinstonHelper.loggerWinstonGlobal.error({
                    collectData: {
                        date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                        location: getCurrentLine(),
                        reqData : {
                            phoneNumber: phoneNumber,
                        }
                    },
                    message : error.stack + '\n'
                })

                return ApiHelper.builFuncResponse(true, 'Get Data Success', {
                    country         : "ID",
                    phoneCountryCode: "62",
                    phoneNational   : "08",
                })
            }
        }

}

export default MainHelper