import { pathConstantImage } from "./../constant/PathConstant";

const path           = require('path')
const multer         = require('multer')
const { v4: uuidv4 } = require('uuid')
require('dotenv').config()

    // REVIEW Upload Collect Image PopCenter
        const uploadImageEmployee = multer.diskStorage({
            destination: function (req, file, cb) {
                cb(null, pathConstantImage.employee)
            },
            filename: function (req, file, cb) {
                cb(null, uuidv4()+path.extname(file.originalname))
            }
        });

        const uploadImageEmployeeFilter = (req, file, cb) => {
            if(
                file.mimetype === 'image/png' ||
                file.mimetype === 'image/jpg' ||
                file.mimetype === 'image/jpeg'
            ){
                cb(null, true)
            } else {
                cb(new Error('Extension Only Allowed png, jpg, jpeg'), false)
            }
        }

        const uploadImageEmployeeSingle = multer({
            storage   : uploadImageEmployee,
            limits: {
                fileSize: Number(process.env.MAX_IMAGE_SIZE)
            },
            fileFilter: uploadImageEmployeeFilter
        }).single('image');

        const uploadImageEmployeeArray = multer({
            storage   : uploadImageEmployee,
            limits: {
                fileSize: Number(process.env.MAX_IMAGE_SIZE)
            },
            fileFilter: uploadImageEmployeeFilter
        }).array('images');



    module.exports = {
        uploadImageEmployeeArray,
        uploadImageEmployeeSingle
    };