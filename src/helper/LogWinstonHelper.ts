import moment = require('moment-timezone');
import momentTz = require('moment-timezone');
import { pathConstantLog } from '../constant/PathConstant';
import { DefaultTimeZone } from '../constant/DefaultConstant';

const path = require('path');
const { v4: uuidv4 } = require('uuid');
require('dotenv').config();
require('winston-daily-rotate-file');
const winston = require('winston');
const SlackHookWinston = require('./transport/SlackHookWinston');

// REVIEW Config Create Logger
const loggerWinstonGlobal = winston.createLogger({
  level: 'silly',
  // format: winston.format.json(),
  // defaultMeta: { service: 'user-service' },
  // handleExceptions: true,
  transports: [
    //
    // - Write all logs with importance level of `error` or less to `error.log`
    // - Write all logs with importance level of `info` or less to `combined.log`
    //

    new SlackHookWinston({
      webhookUrl: process.env.WEBHOOK_SLACK_URL,
      name: '! POPCENTER ALERT !',
      iconEmoji: ':ghost:',
      level: 'info',
      channel: process.env.WEBHOOK_SLACK_CHANNEL,

      formatter: (error) => {
        const { message, level, collectData } = error;

        const currDate = momentTz
          .tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta)
          .format('YYYY-MM-DD HH:mm:ss');

        let finalMessage = `Date: ${currDate}\nLevel: \`${level}\` \nMessage: \`${message}\` \n`;

        if (collectData) {
          finalMessage += `Req Data: \`\`\`${JSON.stringify(
            collectData,
            undefined,
            2,
          )}\`\`\``;
        }
        return {
          text: finalMessage,
        };
      },
    }),

    new winston.transports.Console({
      level: 'error',
    }),
    new winston.transports.Console({
      level: 'info',
    }),
    new winston.transports.DailyRotateFile({
      level: 'error',
      filename: `${pathConstantLog.error}app-error-log`,
      zippedArchive: true,
      maxFiles: '14d',
    }),
    new winston.transports.DailyRotateFile({
      level: 'http',
      filename: `${pathConstantLog.http}app-http-log`,
      zippedArchive: true,
      maxFiles: '10d',
    }),
  ],
  rejectionHandlers: [
    new winston.transports.DailyRotateFile({
      filename: `${pathConstantLog.exception}app-error-exception-log`,
      zippedArchive: true,
      maxFiles: '14d',
    }),
  ],
  exceptionHandlers: [
    new SlackHookWinston({
      webhookUrl: process.env.WEBHOOK_SLACK_URL,
      name: '! POPCENTER ALERT !',
      iconEmoji: ':ghost:',
      channel: process.env.WEBHOOK_SLACK_CHANNEL,
      formatter: (error: any) => {
        const { message } = error;

        return {
          text: `\`[EXCEPTION]\`\n ${message}`,
        };
      },
    }),

    new winston.transports.DailyRotateFile({
      filename: `${pathConstantLog.exception}app-error-exception-log`,
      zippedArchive: true,
      maxFiles: '14d',
    }),
  ],
});

// REVIEW Config Logger Cron
const loggerCron = winston.createLogger({
  level: 'silly',
  transports: [

    new SlackHookWinston({
      webhookUrl: process.env.WEBHOOK_SLACK_URL,
      name      : '! POPCENTER ALERT - (Cron-Node) !',
      iconEmoji : ':alarm_clock:',
      level     : 'info',
      channel   : process.env.WEBHOOK_SLACK_CHANNEL,

      formatter: (error) => {
        const { message, level, collectData } = error;

        const currDate = momentTz
          .tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta)
          .format('YYYY-MM-DD HH:mm:ss');

        let finalMessage = `Date: ${currDate}\nLevel: \`${level}\` \nMessage: \`${message}\` \n`;

        if (collectData) {
          finalMessage += `Req Data: \`\`\`${JSON.stringify(
            collectData,
            undefined,
            2,
          )}\`\`\``;
        }
        return {
          text: finalMessage,
        };
      },
    }),

    new winston.transports.DailyRotateFile({
      level: 'info',
      filename: `${pathConstantLog.infoCron}app-info-cron-log`,
      zippedArchive: true,
      maxFiles: '14d',
    }),
    new winston.transports.DailyRotateFile({
      level: 'error',
      filename: `${pathConstantLog.errorCron}app-error-cron-log`,
      zippedArchive: true,
      maxFiles: '14d',
    }),
  ],
  rejectionHandlers: [
    new winston.transports.DailyRotateFile({
      filename: `${pathConstantLog.exceptionCron}app-error-exception-cron-log`,
      zippedArchive: true,
      maxFiles: '14d',
    }),
  ],
  exceptionHandlers: [
    new winston.transports.DailyRotateFile({
      filename: `${pathConstantLog.exceptionCron}app-error-exception-cron-log`,
      zippedArchive: true,
      maxFiles: '14d',
    }),
  ],
});

module.exports = {
  loggerWinstonGlobal,
  loggerCron,
};
