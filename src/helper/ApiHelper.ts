
class ApiHelper{
    static buildResponse = ((status=false,message='Failed Response',data:any, errors=null)=>{
        data = data ? data : []
        const response = {
            status,
            message,
            errors,
            data
        };
        return response;
    });

    static buildErrResponse = ((status=false,message='Failed Response',errors=null, data=[])=>{
        const response = {
            status,
            message,
            errors,
            data
        };
        return response;
    });

    static builFuncResponse = ((status=false,message='Something Wrong Server',data=null)=>{
        const response = {
            status,
            message,
            data
        };
        return response;
    });

}

export default ApiHelper