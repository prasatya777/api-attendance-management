import * as ValidatorJs from 'validatorjs'

class ValidatorHelper{
    static mainValidator = async(body, rules, customMessages) =>{
        let res = {
            err   : null,
            status: false
        }
        const validation = new ValidatorJs(body, rules, customMessages)

        validation.passes(() => {
            res.err    = null
            res.status = true
        })

        validation.fails(() => {
            res.err    = validation.errors
            res.status = false
        })

        return res
    }
}

export default ValidatorHelper