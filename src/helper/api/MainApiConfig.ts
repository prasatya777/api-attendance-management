import 'reflect-metadata';

import * as express from 'express';
import * as BodyParser from 'body-parser';
import RouteMiddleware from '../../middleware/RouteMiddlewareEmployeeService';

import moment = require('moment');
import momentTz = require('moment-timezone');
import getCurrentLine from 'get-current-line';
const LogWinstonHelper = require('../../helper/LogWinstonHelper');

import * as cors from 'cors';
import { DefaultTimeZone } from './../../constant/DefaultConstant';
import attendanceApp from '../../routes/v1/RouteAttendanceService';
import employeeApp from '../../routes/v1/RouteEmployeeService';

(async () => {

  const funcListening = async (app: express.Express, port: number, task: string) => {
    return new Promise((resolve, reject) => {
      const server = app.listen(port, () => {
        LogWinstonHelper.loggerCron.info({
          collectData: {
            date: momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
            task: task,
            type: 'start-listening',
          },
          message: `Listening ${task} on Port: ${port}`
        });
        
        resolve(server);
      });

      server.on('error', (error) => {
        LogWinstonHelper.loggerWinstonGlobal.error({
          collectData: {
            date: momentTz
              .tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta)
              .format('YYYY-MM-DD HH:mm:ss'),
            location: getCurrentLine(),
            reqData: [],
          },
          message: `Listening ${task} on Port: ${port}` + '\n' + error.stack + '\n',
        });

        reject(error);
      });
      
    });
  };

  await funcListening(attendanceApp, Number(process.env.PORT_ATTENDANCE_SERVICE), 'ListeningAttendanceService');
  await funcListening(employeeApp, Number(process.env.PORT_EMPLOYEE_SERVICE), 'ListeningEmployeeService');

})().then( (e) => {

})
.catch((error) => {
  LogWinstonHelper.loggerWinstonGlobal.error({
    collectData: {
      date: momentTz
        .tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta)
        .format('YYYY-MM-DD HH:mm:ss'),
      location: getCurrentLine(),
      reqData: [],
    },
    message: error.stack + '\n',
  });
});
