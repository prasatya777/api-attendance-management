import { Request, Response } from 'express';
import { verify, sign } from 'jsonwebtoken';
import EmployeeService from '../service/employee/employee.service';

class JwtAuth {
  static verifyJwtToken = async (req: Request, res: Response) => {
    const tokenSplit = req.headers['authorization'].split("Bearer ")
    const token = tokenSplit[1]

    let resVerify = await verify(
      token,
      process.env.TOKEN_KEY_USER,
      (err, user) => {
        if (err)
          return {
            status: false,
            message: 'Session User Login Expired',
            data: null,
          };

        return {
          status: true,
          message: 'Valid Token User',
          data: user,
        };
      }
    );

    return {
      status : resVerify.status,
      message: resVerify.message,
      data   : resVerify.data,
    };
  };

  static verifyJwtTokenWithoutHeader = async (token) => {

    let resVerify = await verify(
      token,
      process.env.TOKEN_KEY_USER,
      (err, user) => {
        if (err)
          return {
            status: false,
            message: 'Session User Login Expired',
            data: null,
          };

        return {
          status: true,
          message: 'Valid Token User',
          data: user,
        };
      }
    );

    return {
      status: resVerify.status,
      message: resVerify.message,
      data: resVerify.data,
    };
  };

  static generateJwtToken = async (userId) => {
    const getEmployee = await EmployeeService.getFirstEmployee({
      idEmployee: userId
    })

    if (!getEmployee.status) {
      return {
        status : false,
        message: 'User Not Found',
        token  : null,
      };
    }

    const accessToken = sign(
      {
        uuidEmployee: getEmployee.data.uuidEmployee,
        userId      : getEmployee.data.id,
        email       : getEmployee.data.email,
        type        : getEmployee.data.type,
      },
      process.env.TOKEN_KEY_USER,
      {
        expiresIn: 1 * Number(process.env.EXP_JWT),
      }
    );

    return {
      status: true,
      message: 'Token Success Generated',
      token: accessToken,
    };
  };

}

export default JwtAuth;
