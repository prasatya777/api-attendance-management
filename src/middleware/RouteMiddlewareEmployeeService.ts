import * as express from 'express';
import { DefaultTimeZone } from '../constant/DefaultConstant';

import ApiHelper from '../helper/ApiHelper';
import JwtAuth from './JwtAuth';

import moment           = require('moment');
import momentTz         = require('moment-timezone');
const  LogWinstonHelper = require('./../../src/helper/LogWinstonHelper');
const  requestIp        = require('request-ip');

class RouteMiddlewareEmployeeService {
  static mainMiddleware = express.Router().use((req, res, next) => {
    // REVIEW Get Log Http Request
    const funGetDataReq = (req) => {
      if (req.method == 'POST') return req.body;
      else if (req.method == 'GET') return req.query;
      else if (req.method == 'PUT') return req.body;
      else if (req.method == 'DELETE') return req.query;
      else [];
    };

    LogWinstonHelper.loggerWinstonGlobal.http({
      collectData: {
        date: momentTz
          .tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta)
          .format('YYYY-MM-DD HH:mm:ss'),
        method: req.method,
        url: req.url.toString(),
        clientIp: requestIp.getClientIp(req),
        reqData: funGetDataReq(req),
      },
      message: req.url.toString(),
    });

    // REVIEW Allowed Url Pass Content Type Application/Json
      const passContentType = [
        '/employees/add',
        '/employees/update'
      ];

    if (req.method == 'POST') {
      if (!passContentType.includes(req.url.toString())) {
        if (req.headers['content-type'] != 'application/json') {
          return res
            .status(422)
            .json(ApiHelper.buildErrResponse(false, 'Missing Header Allowed'));
        }
      }
    }

    // REVIEW Allowed Url For Image
      const splitUrl     = req.url.toString().split('/');
      let   stringConcat = null;
      let   lastString   = null;
      if (splitUrl.length >= 4) {
        stringConcat = splitUrl[1];
        lastString = splitUrl[Number(splitUrl.length) - 1];
      }

      if (lastString && stringConcat == 'image') {
        if (stringConcat == 'image' && lastString.match(/.png\b|.jpg\b|.jpeg/i)) {
          return next();
        }
      }

    if (!req.headers['api-header-auth']) {
      return res
        .status(422)
        .json(ApiHelper.buildErrResponse(false, 'Missing Header Allowed'));
    }

    // REVIEW Validation Value Header
    if (
      req.headers['api-header-auth'] !=
      process.env.API_HEADER_AUTH_EMPLOYEE
    ) {
      return res
        .status(400)
        .json(ApiHelper.buildErrResponse(false, 'Header Auth Unauthorized'));
    }

    return next();
  });

  static tokenUserMiddleware = express.Router().use(async (req, res, next) => {
    if (!req.headers['authorization']) {
      return res
        .status(422)
        .json(ApiHelper.buildErrResponse(false, 'Missing Token User'));
    }

    const resTokenUser = await JwtAuth.verifyJwtToken(req, res);
    if (!resTokenUser.status) {
      return res
        .status(422)
        .json(ApiHelper.buildErrResponse(false, resTokenUser.message));
    }

    return next();
  });
}

export default RouteMiddlewareEmployeeService;
