import { Request, Response } from 'express';
import ApiHelper from '../helper/ApiHelper';
const  { v4: uuidv4 }   = require('uuid')

class GenerateController {

    static generateUuid = async (req: Request, res: Response) => {
    return res
      .status(200)
      .json(ApiHelper.buildResponse(true, 'Get Employee Detail Success', {
        uuid: await uuidv4()
      }));
    };

}
export default GenerateController;
