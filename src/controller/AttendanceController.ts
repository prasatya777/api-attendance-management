import { Request, Response } from 'express';
import getCurrentLine from 'get-current-line';
import ApiHelper from '../helper/ApiHelper';
import { DefaultTimeZone } from '../constant/DefaultConstant';
const LogWinstonHelper = require('./../helper/LogWinstonHelper');

import moment    = require('moment');
import momentTz  = require('moment-timezone');
import { attendanceType } from '../constant/AttendanceConstant';
import ValidatorHelper from '../helper/ValidateHelper';
import AttendanceService from '../service/employee/attendance.service';
const  requestIp = require('request-ip');


class AttendanceController {
    static addAttendance = async (req: Request, res: Response) => {
        // REVIEW Get Log Http Request Special For MultipartFormData
          const funGetDataReq = (req) => {
            if (req.method == 'POST') return req.body;
            else if (req.method == 'GET') return req.query;
            else if (req.method == 'PUT') return req.body;
            else if (req.method == 'DELETE') return req.query;
            else [];
          };
      
          LogWinstonHelper.loggerWinstonGlobal.http({
            collectData: {
              date: momentTz
                .tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta)
                .format('YYYY-MM-DD HH:mm:ss'),
              method  : req.method,
              url     : req.url.toString(),
              clientIp: requestIp.getClientIp(req),
              reqData : funGetDataReq(req),
            },
            message: req.url.toString(),
          });
    
        // REVIEW Validation Input
          const ruleValidator = {
            uuidEmployee: 'required|string',
            type        : ['required', { in: Object.values(attendanceType) }]
          };
    
          let resValid = await ValidatorHelper.mainValidator(
            req.body,
            ruleValidator,
            {}
          );
          if (!resValid.status) {    
            return res
              .status(422)
              .json(
                ApiHelper.buildErrResponse(
                  false,
                  'Validation Error',
                  Object.values(resValid.err)
                )
              );
          }
        
        // REVIEW Config Date
          const funcDateTime = () => {
            const unixTime = moment()
            return {
                dateTime        : unixTime.clone().format('YYYY-MM-DD'),
                unixtimeDateTime: unixTime.clone().format('x')
            }
          }

          const resDateTime = await funcDateTime()

        // REVIEW Validation Type
          const resAttendanceCheck = await AttendanceService.getFirstAttendance({
            uuidEmployee: req.body.uuidEmployee,
            type: attendanceType[req.body.type],
            dateTime: resDateTime.dateTime,
          })

          if(resAttendanceCheck.status){
            return res
            .status(422)
            .json(ApiHelper.buildResponse(false, `Your already ${attendanceType[req.body.type]}`, null));
          }
        
        // REVIEW Set Value Add Attendance
          const payloadAddAttendance = {
            uuidEmployee: req.body.uuidEmployee,
            type        : attendanceType[req.body.type],
            ...resDateTime
          };
    
        // REVIEW Process Add Attendance
          const resAddAttendance = await AttendanceService.addAttendance(payloadAddAttendance)
          if(!resAddAttendance.status){
            return res
            .status(422)
            .json(ApiHelper.buildErrResponse(false, resAddAttendance.message));
          }
    
        return res
          .status(200)
          .json(ApiHelper.buildResponse(true, `Add Attendance ${attendanceType[req.body.type]} Success`, null));
    };

    static getComnbineAttendance = async (req: Request, res: Response) => {
        // REVIEW Get Log Http Request Special For MultipartFormData
          const funGetDataReq = (req) => {
            if (req.method == 'POST') return req.body;
            else if (req.method == 'GET') return req.query;
            else if (req.method == 'PUT') return req.body;
            else if (req.method == 'DELETE') return req.query;
            else [];
          };
      
          LogWinstonHelper.loggerWinstonGlobal.http({
            collectData: {
              date: momentTz
                .tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta)
                .format('YYYY-MM-DD HH:mm:ss'),
              method  : req.method,
              url     : req.url.toString(),
              clientIp: requestIp.getClientIp(req),
              reqData : funGetDataReq(req),
            },
            message: req.url.toString(),
          });

        // REVIEW Param Filter
          const paramFilter = {
            uuidEmployee: req.query.uuidEmployee ? req.query.uuidEmployee.toString(): null,
            dateTime    : req.query.dateTime ? req.query.dateTime.toString()        : null,
          }
    
        // REVIEW Process Get Attendance
          const resGetAttendance = await AttendanceService.getManyCombineAttendance(paramFilter)
          if(!resGetAttendance.status){
            return res
            .status(422)
            .json(ApiHelper.buildErrResponse(false, resGetAttendance.message));
          }

        // REVIEW Reformat Date
          const funcReformatDate = (date) => {
            if(date){
                return moment(date, 'YYYY-MM-DD').format('DD MMM YYYY')
            }
            return '-'
          }

          const funcReformatDateUnix = (unixdate) => {
            if(unixdate){
                return moment(Number(unixdate)).format('DD MMM YYYY HH:mm:ss')
            }
            return '-'
          }

          const reformatData = await Promise.all(resGetAttendance.data.map(async(val)=>{
            return {
                uuidEmployee: val.uuidEmployee,
                dateTime    : await funcReformatDate(val.dateTime),
                clockInType : val.clockin_type,
                clockInTime : await funcReformatDateUnix(val.clockin_unixtime),
                clockOutType: val.clockout_type,
                clockOutTime: await funcReformatDateUnix(val.clockout_unixtime),
            }
          }))
    
        return res
          .status(200)
          .json(ApiHelper.buildResponse(true, `Get History Attendance Success`, reformatData));
    };
}
export default AttendanceController;
