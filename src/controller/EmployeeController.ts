import { Request, Response } from 'express';
import { genSaltSync, hashSync, compare } from 'bcryptjs';
import getCurrentLine from 'get-current-line';
import ApiHelper from '../helper/ApiHelper';
import { DefaultTimeZone } from '../constant/DefaultConstant';
import ToolFsNodeJshelper from '../helper/ToolFsNodeJshelper';
import { pathConstantImage, pathConstantImageViewLink } from '../constant/PathConstant';
import { employeeStatus, employeeType } from '../constant/EmployeeConstant';
import ValidatorHelper from '../helper/ValidateHelper';
import MainHelper from '../helper/MainHelper';

import moment           = require('moment');
import momentTz         = require('moment-timezone');
import EmployeeService from '../service/employee/employee.service';
import JwtAuth from '../middleware/JwtAuth';
const  LogWinstonHelper = require('./../helper/LogWinstonHelper');
const  requestIp        = require('request-ip');
const  { v4: uuidv4 }   = require('uuid')

class EmployeeController {

    static employeeAdd = async (req: Request & { files: any }, res: Response) => {
        // REVIEW Get Log Http Request Special For MultipartFormData
          const funGetDataReq = (req) => {
            if (req.method == 'POST') return req.body;
            else if (req.method == 'GET') return req.query;
            else if (req.method == 'PUT') return req.body;
            else if (req.method == 'DELETE') return req.query;
            else [];
          };
      
          LogWinstonHelper.loggerWinstonGlobal.http({
            collectData: {
              date: momentTz
                .tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta)
                .format('YYYY-MM-DD HH:mm:ss'),
              method  : req.method,
              url     : req.url.toString(),
              clientIp: requestIp.getClientIp(req),
              reqData : funGetDataReq(req),
            },
            message: req.url.toString(),
          });
        
        // REVIEW Get Image Name
            let collectImage = []
            try {
                collectImage = Object.values(req.files).map((v)=>Object(v).filename)
                if(collectImage.length <= 0){
                    return res.status(400).json(ApiHelper.buildErrResponse(false, 'Invalid Image'));
                }
            } catch (error) {
        
                const funGetDataReq = (req) => {
                    if(req.method == 'POST') return req.body 
                    else if(req.method == 'GET') return req.query
                    else if(req.method == 'PUT') return req.body
                    else if(req.method == 'DELETE') return req.query
                    else []
                }
            
                LogWinstonHelper.loggerWinstonGlobal.error({
                    collectData: {
                    date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                    location: getCurrentLine(),
                    reqData : funGetDataReq(req)
                    },
                    message : error.stack + '\n'
                })
            
                return res.status(422).json(ApiHelper.buildErrResponse(false, 'Invalid Image'));
            }

        // REVIEW FuncDeleteImage
            const funcDeleteImage = async(collectImage)=>{
                if(collectImage.length > 0){
                    await ToolFsNodeJshelper.deleteFile(pathConstantImage.employee,collectImage)
                }

                return true
            }
    
        // REVIEW Validation Input
          const ruleValidator = {
            uuidEmployee: 'required|string',
            name        : 'required|string',
            email       : 'required|email',
            positionJob : 'required|string',
            phone       : 'required|string',
            status      : ['required', { in: Object.values(employeeStatus) }],
            type        : ['required', { in: Object.values(employeeType) }],
            password    : 'required',
          };
    
          let resValid = await ValidatorHelper.mainValidator(
            req.body,
            ruleValidator,
            {}
          );
          if (!resValid.status) {
            await funcDeleteImage(collectImage)
    
            return res
              .status(422)
              .json(
                ApiHelper.buildErrResponse(
                  false,
                  'Validation Error',
                  Object.values(resValid.err)
                )
              );
          }

        const funcConvertPhone = req.body.phone ? await MainHelper.getCountryCodeFromPhoneNumber(req.body.phone).then(async(val)=> {
            if(val.status){
                return await MainHelper.reFormatPhoneNumberByCountry(req.body.phone, val.data.country).then((val2)=> {
                if(val2.status){
                    return val2.data.phoneCountryCode.phone
                }
                    return req.body.phone
                })
            }
            return req.body.phone
        }) : null
        
        // REVIEW Set Value Add Employee
          const salt = await genSaltSync(10);
          const payloadAddEmployee = {
            uuidEmployee: req.body.uuidEmployee,
            name        : req.body.name,
            email       : req.body.email,
            positionJob : req.body.positionJob,
            phone       : funcConvertPhone,
            status      : employeeStatus[req.body.status],
            type        : employeeType[req.body.type],
            password    : await hashSync(req.body.password, salt),
            image       : collectImage,
          };
    
    
        // REVIEW Process Add Employee
          const resAddEmployee = await EmployeeService.addEmployee(payloadAddEmployee)
          if(!resAddEmployee.status){

            await funcDeleteImage(collectImage)

            return res
            .status(422)
            .json(ApiHelper.buildErrResponse(false, resAddEmployee.message));
          }
    
        return res
          .status(200)
          .json(ApiHelper.buildResponse(true, 'Add PopCenter Success', null));
    };

    static employeeUpdate = async (req: Request & { files: any }, res: Response) => {
      // REVIEW Get Log Http Request Special For MultipartFormData
        const funGetDataReq = (req) => {
          if (req.method == 'POST') return req.body;
          else if (req.method == 'GET') return req.query;
          else if (req.method == 'PUT') return req.body;
          else if (req.method == 'DELETE') return req.query;
          else [];
        };
    
        LogWinstonHelper.loggerWinstonGlobal.http({
          collectData: {
            date: momentTz
              .tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta)
              .format('YYYY-MM-DD HH:mm:ss'),
            method  : req.method,
            url     : req.url.toString(),
            clientIp: requestIp.getClientIp(req),
            reqData : funGetDataReq(req),
          },
          message: req.url.toString(),
        });
        
      // REVIEW Get Image Name
        let collectImage = []
        try {
          collectImage = Object.values(req.files).map((v)=>Object(v).filename)
        } catch (error) {
  
          const funGetDataReq = (req) => {
            if(req.method == 'POST') return req.body 
            else if(req.method == 'GET') return req.query
            else if(req.method == 'PUT') return req.body
            else if(req.method == 'DELETE') return req.query
            else []
          }
  
          LogWinstonHelper.loggerWinstonGlobal.error({
            collectData: {
              date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
              location: getCurrentLine(),
              reqData : funGetDataReq(req)
            },
            message : error.stack + '\n'
          })
  
          return res.status(400).json(ApiHelper.buildErrResponse(false, 'Invalid Image'));
        }

      // REVIEW FuncDeleteImage
        const funcDeleteImage = async(collectImage)=>{
            if(collectImage.length > 0){
                await ToolFsNodeJshelper.deleteFile(pathConstantImage.employee,collectImage)
            }

            return true
        }
  
      // REVIEW Validation Input
        const ruleValidator = {
          uuidEmployee: 'required|string',
          name        : 'required|string',
          email       : 'required|email',
          positionJob : 'required|string',
          phone       : 'required|string',
          status      : ['required', { in: Object.values(employeeStatus) }],
          type        : ['required', { in: Object.values(employeeType) }],
          password    : 'string',
        };

        let resValid = await ValidatorHelper.mainValidator(
          req.body,
          ruleValidator,
          {}
        );
        if (!resValid.status) {
          await funcDeleteImage(collectImage)
  
          return res
            .status(422)
            .json(
              ApiHelper.buildErrResponse(
                false,
                'Validation Error',
                Object.values(resValid.err)
              )
            );
        }
  
      // REVIEW Check uuidEmployee
        const resUuidExist = await EmployeeService.getFirstEmployee(
          {
            uuidEmployee: req.body.uuidEmployee
          }
        )
        if(!resUuidExist.status){
          return res
          .status(404)
          .json(ApiHelper.buildErrResponse(false, 'Data Employee Not Found'));
        }

        const funcConvertPhone = req.body.phone ? await MainHelper.getCountryCodeFromPhoneNumber(req.body.phone).then(async(val)=> {
          if(val.status){
              return await MainHelper.reFormatPhoneNumberByCountry(req.body.phone, val.data.country).then((val2)=> {
              if(val2.status){
                  return val2.data.phoneCountryCode.phone
              }
                  return req.body.phone
              })
          }
          return req.body.phone
        }) : null
      
      // REVIEW Set Value Update Employee
        const salt = await genSaltSync(10);
        const payloadUpdateEmployee = {
          ...(req.body.name && { name: req.body.name }),
          ...(req.body.email && { email: req.body.email }),
          ...(req.body.positionJob && { positionJob: req.body.positionJob }),
          ...(req.body.phone && { phone: funcConvertPhone }),
          ...(req.body.status && { status: employeeStatus[req.body.status] }),
          ...(req.body.type && { type: employeeType[req.body.type] }),
          ...(req.body.password && req.body.password != '' && req.body.password != null && { password: await hashSync(req.body.password, salt) }),
          ...(Array.isArray(collectImage) && collectImage.length > 0 && { image: collectImage }),
        };
  
      // REVIEW Process Update Employee
        const resUpdateEmployee = await EmployeeService.updateEmployee(payloadUpdateEmployee, resUuidExist.data.id)
        if(!resUpdateEmployee.status){
          return res
          .status(422)
          .json(ApiHelper.buildErrResponse(false, resUpdateEmployee.message));
        }
  
      return res
        .status(200)
        .json(ApiHelper.buildResponse(true, 'Update Employee Success', null));
    };

    static employeeDetail = async (req: Request, res: Response) => {  
      // REVIEW Validation Input
        const ruleValidator = {
          uuidEmployee  : 'required'
        };
  
        let resValid = await ValidatorHelper.mainValidator(
          req.query,
          ruleValidator,
          {}
        );
        if (!resValid.status) {
  
          return res
            .status(400)
            .json(
              ApiHelper.buildErrResponse(
                false,
                'Validation Error',
                Object.values(resValid.err)
              )
            );
        }
  
      // REVIEW Get Detail Employee
        const resDetailEmployee = await EmployeeService.getFirstEmployee({
          uuidEmployee: req.query.uuidEmployee
        })
        if(!resDetailEmployee.status){
          return res
          .status(404)
          .json(ApiHelper.buildErrResponse(false, resDetailEmployee.message));
        }
  
      // REVIEW Set Final Value
        let cloneDetail = {...Object.assign({}, resDetailEmployee.data)}
        delete cloneDetail.password

        const resFinal = {
            ...cloneDetail,
            image: cloneDetail.image.map((v) => {if(v){return `${process.env.URL_API_EMPLOYEE}/${pathConstantImageViewLink.employee}${v}`}}),
        }
      return res
        .status(200)
        .json(ApiHelper.buildResponse(true, 'Get Employee Detail Success', resFinal));
    };

    static employeeLogin = async (req: Request, res: Response) => {
      // REVIEW Validation Input
        const ruleValidator = {
          email   : 'required|string|email',
          password: 'required',
        };
  
        let resValid = await ValidatorHelper.mainValidator(
          req.body,
          ruleValidator,
          {}
        );
  
        if (!resValid.status) {
          return res
            .status(400)
            .json(
              ApiHelper.buildErrResponse(
                false,
                'Validation Error',
                Object.values(resValid.err)
              )
            );
        }
  
      // REVIEW Get Data Employee by Email
        const resGetEmployee = await EmployeeService.getFirstEmployee({
          email: req.body.email
        })
  
        if (resGetEmployee.status) {
          const validPassword = await compare(req.body.password, resGetEmployee.data.password);
          if (validPassword) {
            // REVIEW Generate Token
              const tokenUser = await JwtAuth.generateJwtToken(
                resGetEmployee.data.id
              );
              if (!tokenUser.status) {
                return res
                  .status(422)
                  .json(ApiHelper.buildErrResponse(false, tokenUser.message));
              }
            
            // REVIEW Decrypt Tokrn User
              const resTokenUser = await JwtAuth.verifyJwtTokenWithoutHeader(tokenUser.token);
              if (!resTokenUser.status) {
                return res
                  .status(422)
                  .json(ApiHelper.buildErrResponse(false, resTokenUser.message));
              }
  
            const resUpdateToken = await EmployeeService.updateEmployee(
              {
                token: tokenUser.token
              },
              resGetEmployee.data.id,
            );
            if (!resUpdateToken.status) {
              return res
                .status(422)
                .json(ApiHelper.buildErrResponse(false, resUpdateToken.message));
            }
  
            delete resUpdateToken.data.password;
            delete resUpdateToken.data.delete;
  
            return res.status(200).json(
              ApiHelper.buildResponse(true, 'Login Success', {
                ...resUpdateToken.data,
              })
            );
          } else {
            return res
              .status(422)
              .json(ApiHelper.buildErrResponse(false, 'Invalid Password'));
          }
        }

      return res
        .status(404)
        .json(ApiHelper.buildErrResponse(false, 'Employee does not exist'));
    };

    static employeeMe = async (req: Request, res: Response) => {
      // REVIEW Validation and Parse Token
        const resJwt = await JwtAuth.verifyJwtToken(req, res);
        if (!resJwt.status) {
          return res
            .status(400)
            .json(ApiHelper.buildErrResponse(false, resJwt.message));
        }
  
      // REVIEW Get Detail Employee
      const resDetailEmployee = await EmployeeService.getFirstEmployee({
        idEmployee: resJwt.data.userId
      })
      if(!resDetailEmployee.status){
        return res
        .status(404)
        .json(ApiHelper.buildErrResponse(false, resDetailEmployee.message));
      }

    // REVIEW Set Final Value
      let cloneDetail = {...Object.assign({}, resDetailEmployee.data)}
      delete cloneDetail.password

      const resFinal = {
          ...cloneDetail,
          image: cloneDetail.image.map((v) => {if(v){return `${process.env.URL_API_EMPLOYEE}/${pathConstantImageViewLink.employee}${v}`}}),
      }
    return res
      .status(200)
      .json(ApiHelper.buildResponse(true, 'Get Employee Detail Success', resFinal));
    };

    static configEmployeeLevel = async (req: Request, res: Response) => {
      // REVIEW Validation and Parse Token
        const resJwt = await JwtAuth.verifyJwtToken(req, res);
        if (!resJwt.status) {
          return res
            .status(400)
            .json(ApiHelper.buildErrResponse(false, resJwt.message, {
              status: false
            }));
        }
  
      // REVIEW Get Detail Employee
        const resDetailEmployee = await EmployeeService.getFirstEmployee({
          idEmployee: resJwt.data.userId
        })
        if(!resDetailEmployee.status){
          return res
          .status(404)
          .json(ApiHelper.buildErrResponse(false, resDetailEmployee.message, {
            status: false
          }));
        }

      // REVIEW Hardcode Access Menu
        if(resDetailEmployee.data.type == employeeType.EMPLOYEE){
          const collectAllow = [
            'menuDashboard',
            'menuAttendanceEmployee',
            'menuHistoryAttendanceEmployee',
            'menuProfileEmployee'
          ]

          if(collectAllow.includes(req.query.nameConfig.toString())){
            return res
              .status(200)
              .json(ApiHelper.buildResponse(true, 'User Access Granted', {
                status: true
              }))
          }
        }else if(resDetailEmployee.data.type == employeeType.ADMINHRD){
          const collectAllow = [
            'menuDashboard',
            'menuAttendanceEmployee',
            'menuHistoryAttendanceEmployee',
            'menuProfileEmployee',
            'menuEmployeeHr'
          ]

          if(collectAllow.includes(req.query.nameConfig.toString())){
            return res
              .status(200)
              .json(ApiHelper.buildResponse(true, 'User Access Granted', {
                status: true
              }))
          }
        }

        return res
        .status(403)
        .json(ApiHelper.buildResponse(false, 'User Access Denied', {
          status: false
        }))

    };

    static employeeList = async (req: Request, res: Response) => {
      try {
  
        // REVIEW Define Column DataTables to Query
          const collectColumnDataTables = {
            0 : 'employee.id',
            1 : 'employee.name',
            2 : 'employee.phone',
            3 : 'employee.email'
          }
        
        // REVIEW Set Param Filter
          interface structureFilter{
            limitColumn?: number,
            offsetColumn?: number,
            orderColumn?:string,
            typeOrderColumn?:string,
            selectType?:string,
            collectSelect?:any
  
          }

        // REVIEW Get All Data Without Limit Pagination
          let collectParamFilter : structureFilter = {
            collectSelect: [
              'employee.id AS id'
            ]
          } 
          collectParamFilter.selectType = 'count_data'
  
          const resCountData = await EmployeeService.getDataTableEmployee(collectParamFilter);
          if (!resCountData.status) {
            return res
              .status(400)
              .json(ApiHelper.buildErrResponse(false, resCountData.message, resCountData.data, []));
          }
  
          // REVIEW Filter PopCenterId
            const totalAllData = Number(resCountData.data.length)
  
        // REVIEW Set Param Pagination
            collectParamFilter.limitColumn     = req.query.show_record ? Number(req.query.show_record) : 10,
            collectParamFilter.offsetColumn    = req.query.page ? (Number(req.query.page) > 1 ? ((Number(req.query.page)-1) * Number(req.query.show_record ? Number(req.query.show_record) : 10)) : 0): 0,
            collectParamFilter.orderColumn     = req.query.order_column ? collectColumnDataTables[Number(req.query.order_column)]                                                                         : collectColumnDataTables[0],
            collectParamFilter.typeOrderColumn = req.query.order_sort ? req.query.order_sort.toString().toUpperCase()                                                                                     : 'ASC',
            collectParamFilter.selectType      = 'get_data'
  
          let paginateDetail = {
            'page'               : req.query.page ? Number(req.query.page) : 1,
            'show_record'        : collectParamFilter.limitColumn,
            'start_record'       : Number(collectParamFilter.offsetColumn) + 1,
            'order_column'       : req.query.order_column ? Number(req.query.order_column): 0,
            'order_sort'         : collectParamFilter.typeOrderColumn,
            'total_all_data'     : totalAllData,
            'total_data_paginate': 0,
          }
  
        // REVIEW Set Select Query Param Filter
            collectParamFilter.collectSelect = [
              'employee.id AS id', 
              'employee.uuidEmployee AS uuidEmployee', 
              'employee.name AS name', 
              'employee.phone AS phone', 
              'employee.email AS email',
              'employee.positionJob AS positionJob',
              'employee.status AS status',
              'employee.type AS type'
            ]
  
        // REVIEW Get Data 
          const resUserFilterData = await EmployeeService.getDataTableEmployee(collectParamFilter);
          if (!resUserFilterData.status) {
            return res
              .status(400)
              .json(ApiHelper.buildErrResponse(false, resUserFilterData.message, null, []));
          }
          paginateDetail.total_data_paginate = Number(resUserFilterData.data.length)

  
          let returnResponse = {
            'paginate'   : paginateDetail,
            'data'       : resUserFilterData.data
          }
            
          return res
            .status(200)
            .json(ApiHelper.buildResponse(true, 'Data Founded', returnResponse));
  
      } catch (error) {
  
        const funGetDataReq = (req) => {
          if(req.method == 'POST') return req.body 
          else if(req.method == 'GET') return req.query
          else if(req.method == 'PUT') return req.body
          else if(req.method == 'DELETE') return req.query
          else []
        }
  
        LogWinstonHelper.loggerWinstonGlobal.error({
          collectData: {
            date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
            location: getCurrentLine(),
            reqData : funGetDataReq(req)
          },
          message : error.stack + '\n'
        })
  
        return res
          .status(400)
          .json(ApiHelper.buildErrResponse(false, 'Data Not Found', error, []));
      }
    };


}
export default EmployeeController;
