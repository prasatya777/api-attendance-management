import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateAttendanceTable1693391156607 implements MigrationInterface {
    name = 'CreateAttendanceTable1693391156607'

    public async up(queryRunner: QueryRunner): Promise<void> {
        if ( !await queryRunner.hasTable('attendance') ) await queryRunner.query(`CREATE TABLE \`attendance\` (\`id\` int NOT NULL AUTO_INCREMENT, \`uuid_employee\` varchar(255) NOT NULL, \`type\` varchar(255) NOT NULL, \`date_time\` date NOT NULL, \`unixtime_date_time\` bigint NULL DEFAULT '0', \`created\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`delete\` datetime(6) NULL, INDEX \`attendance-uuidEmployee-idx\` (\`uuid_employee\`), INDEX \`attendance-type-idx\` (\`type\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        if ( await queryRunner.hasTable('attendance') ) {
            await queryRunner.query(`START TRANSACTION`);

            try{
                if ( 
                    await queryRunner.hasColumn('attendance', 'type') 
                ){
                    const collectConstraint     = await queryRunner.query(`SELECT TABLE_NAME, CONSTRAINT_TYPE, CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = "attendance" `)
                    const collectConstraintName = await Array.from(new Set(await collectConstraint.map((val)=> val.CONSTRAINT_NAME)))
                    if(await collectConstraintName.includes('attendance-type-idx')){
                        await queryRunner.query(`DROP INDEX \`attendance-type-idx\` ON \`attendance\``);
                    }
                }

                if ( 
                    await queryRunner.hasColumn('attendance', 'uuid_employee') 
                ){
                    const collectConstraint     = await queryRunner.query(`SELECT TABLE_NAME, CONSTRAINT_TYPE, CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = "attendance" `)
                    const collectConstraintName = await Array.from(new Set(await collectConstraint.map((val)=> val.CONSTRAINT_NAME)))
                    if(await collectConstraintName.includes('attendance-uuidEmployee-idx')){
                        await queryRunner.query(`DROP INDEX \`attendance-uuidEmployee-idx\` ON \`attendance\``);
                    }
                }
    
                await queryRunner.query(`DROP TABLE \`attendance\``);
                
                await queryRunner.query(`COMMIT`);
            } catch (error) {
                await queryRunner.query(`ROLLBACK`);
            }

        }
    }

}
