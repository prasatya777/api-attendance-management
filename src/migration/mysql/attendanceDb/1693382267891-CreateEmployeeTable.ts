import { MigrationInterface, QueryRunner } from "typeorm";

export class CreateEmployeeTable1693382267891 implements MigrationInterface {
    name = 'CreateEmployeeTable1693382267891'

    public async up(queryRunner: QueryRunner): Promise<void> {
        if ( !await queryRunner.hasTable('employee') ) await queryRunner.query(`CREATE TABLE \`employee\` (\`id\` int NOT NULL AUTO_INCREMENT, \`uuid_employee\` varchar(255) NOT NULL, \`name\` varchar(255) NOT NULL, \`email\` varchar(255) NOT NULL, \`position_job\` varchar(255) NOT NULL, \`phone\` varchar(255) NULL, \`status\` varchar(255) NOT NULL DEFAULT 'INACTIVE', \`type\` varchar(255) NOT NULL, \`password\` varchar(255) NOT NULL, \`image\` json NULL, \`last_login\` datetime NULL, \`token\` mediumtext NULL, \`created\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`delete\` datetime(6) NULL, INDEX \`employee-email-idx\` (\`email\`), INDEX \`employee-phone-idx\` (\`phone\`), UNIQUE INDEX \`IDX_0f9b8c7894aec98b40f0e0aecd\` (\`uuid_employee\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        if ( await queryRunner.hasTable('employee') ) {
            await queryRunner.query(`START TRANSACTION`);

            try{
                if ( 
                    await queryRunner.hasColumn('employee', 'uuid_employee') 
                ){
                    const collectConstraint     = await queryRunner.query(`SELECT TABLE_NAME, CONSTRAINT_TYPE, CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = "employee" `)
                    const collectConstraintName = await Array.from(new Set(await collectConstraint.map((val)=> val.CONSTRAINT_NAME)))
                    if(await collectConstraintName.includes('IDX_0f9b8c7894aec98b40f0e0aecd')){
                        await queryRunner.query(`DROP INDEX \`IDX_0f9b8c7894aec98b40f0e0aecd\` ON \`employee\``);
                    }
                }

                if ( 
                    await queryRunner.hasColumn('employee', 'phone') 
                ){
                    const collectConstraint     = await queryRunner.query(`SELECT TABLE_NAME, CONSTRAINT_TYPE, CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = "employee" `)
                    const collectConstraintName = await Array.from(new Set(await collectConstraint.map((val)=> val.CONSTRAINT_NAME)))
                    if(await collectConstraintName.includes('employee-phone-idx')){
                        await queryRunner.query(`DROP INDEX \`employee-phone-idx\` ON \`employee\``);
                    }
                }

                if ( 
                    await queryRunner.hasColumn('employee', 'email') 
                ){
                    const collectConstraint     = await queryRunner.query(`SELECT TABLE_NAME, CONSTRAINT_TYPE, CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = "employee" `)
                    const collectConstraintName = await Array.from(new Set(await collectConstraint.map((val)=> val.CONSTRAINT_NAME)))
                    if(await collectConstraintName.includes('employee-email-idx')){
                        await queryRunner.query(`DROP INDEX \`employee-email-idx\` ON \`employee\``);
                    }
                }
    
                await queryRunner.query(`DROP TABLE \`employee\``);
                
                await queryRunner.query(`COMMIT`);
            } catch (error) {
                await queryRunner.query(`ROLLBACK`);
            }

        }
    }

}
