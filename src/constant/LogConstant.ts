export enum nameConstantLog {
    error         = 'app-error-log',
    http          = 'app-http-log',
    exception     = 'app-error-exception-log',
    infoCron      = 'app-info-cron-log',
    errorCron     = 'app-error-cron-log',
    exceptionCron = 'app-error-exception-cron-log',
}