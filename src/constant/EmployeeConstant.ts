export enum employeeStatus {
    ACTIVE   = 'ACTIVE',
    INACTIVE = 'INACTIVE'
}

export enum employeeType {
    ADMINHRD = 'ADMINHRD',
    EMPLOYEE = 'EMPLOYEE'
}