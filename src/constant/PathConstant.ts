export enum pathConstantLog {
    error         = './src/log/error/',
    exception     = './src/log/error/exception/',
    http          = './src/log/http/',
    info          = './src/log/info/',
    errorCron     = './src/log/cron/error/',
    exceptionCron = './src/log/cron/error/exception/',
    infoCron      = './src/log/cron/info/',
}

export enum pathConstantImage {
    employee = './src/storage/image/employees/'
}

export enum pathConstantImageViewLink {
    employee = 'api/v1/image/employee/',
}