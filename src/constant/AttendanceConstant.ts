export enum attendanceType {
    CLOCKIN  = 'CLOCKIN',
    CLOCKOUT = 'CLOCKOUT'
}