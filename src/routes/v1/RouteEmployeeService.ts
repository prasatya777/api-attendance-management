import * as express from 'express';
import * as cors from 'cors';
import EmployeeController from '../../controller/EmployeeController';
import moment   = require('moment');
import momentTz = require('moment-timezone');
const LogWinstonHelper    = require('../../helper/LogWinstonHelper');
const { DefaultTimeZone } = require('./../../constant/DefaultConstant');
import getCurrentLine from 'get-current-line';
import bodyParser = require('body-parser');
import { pathConstantImage } from '../../constant/PathConstant';
import ApiHelper from '../../helper/ApiHelper';
import RouteMiddlewareEmployeeService from '../../middleware/RouteMiddlewareEmployeeService';
import GenerateController from '../../controller/GenerateController';
const MulterlHelper = require('./../../helper/MulterHelper')

const employeeApp = express();
const multer = require('multer')


try {
    // REVIEW Set Cors Blocked
        const corsOptions: cors.CorsOptions = {
            origin: '*',
            allowedHeaders: ['api-header-auth', 'content-type', 'authorization'],
        };
        
            employeeApp.use(cors(corsOptions));
            employeeApp.use(bodyParser.json());

        const routeMiddleware = express.Router();
        routeMiddleware.use(RouteMiddlewareEmployeeService.mainMiddleware);
            
        const apiV1Router = express.Router();
        apiV1Router.use('/', routeMiddleware);
    
    // REVIEW Set Endpoint Login
    apiV1Router.post('/employees/login', EmployeeController.employeeLogin);

    // REVIEW Get Image Url
        const imageUrl = express.Router();
            imageUrl.use('/employee', express.static(pathConstantImage.employee));
        apiV1Router.use('/image', imageUrl);

    const employees = express.Router();
        employees.use(RouteMiddlewareEmployeeService.tokenUserMiddleware);
        employees.post('/add', async(req:express.Request & { files: any }, res) => {
            MulterlHelper.uploadImageEmployeeArray(req, res, async(err) => {
                if (err instanceof multer.MulterError) {
                    if(err.code == 'LIMIT_FILE_SIZE')  return res.status(422).json(ApiHelper.buildErrResponse(false, err.message+' Max '+Number(process.env.MAX_IMAGE_SIZE)/1000+ 'KB', err.storageErrors));
                    return res.status(422).json(ApiHelper.buildErrResponse(false, err.message, err.storageErrors));
                } else if (err) {
                    return res.status(500).json(ApiHelper.buildErrResponse(false, err.message, err.storageErrors));
                }
                EmployeeController.employeeAdd(req, res)
            })
        });
        employees.put('/update', async(req:express.Request & { files: any }, res) => {
            MulterlHelper.uploadImageEmployeeArray(req, res, async(err) => {
                if (err instanceof multer.MulterError) {
                    if(err.code == 'LIMIT_FILE_SIZE')  return res.status(422).json(ApiHelper.buildErrResponse(false, err.message+' Max '+Number(process.env.MAX_IMAGE_SIZE)/1000+ 'KB', err.storageErrors));
                    return res.status(422).json(ApiHelper.buildErrResponse(false, err.message, err.storageErrors));
                } else if (err) {
                    return res.status(500).json(ApiHelper.buildErrResponse(false, err.message, err.storageErrors));
                }
                
                EmployeeController.employeeUpdate(req, res);
            })
        });
        employees.get('/detail', EmployeeController.employeeDetail);
        employees.get('/list', EmployeeController.employeeList);
        employees.get('/me', EmployeeController.employeeMe);
        employees.get('/configUserLevel', EmployeeController.configEmployeeLevel);
    apiV1Router.use('/employees', employees);

    // REVIEW Generate 
        const generate = express.Router();
            generate.get('/uuid', GenerateController.generateUuid);
        apiV1Router.use('/generate', generate);
    
    employeeApp.use('/api/v1', apiV1Router);


}
catch(error) {
  LogWinstonHelper.loggerWinstonGlobal.error({
    collectData: {
      date: momentTz
        .tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta)
        .format('YYYY-MM-DD HH:mm:ss'),
      location: getCurrentLine(),
      reqData: [],
    },
    message: error.stack + '\n',
  });
};

export default employeeApp;

