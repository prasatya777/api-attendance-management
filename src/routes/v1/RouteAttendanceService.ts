import * as express from 'express';
import * as cors from 'cors';
import AttendanceController from '../../controller/AttendanceController';
import moment   = require('moment');
import momentTz = require('moment-timezone');
const LogWinstonHelper    = require('../../helper/LogWinstonHelper');
const { DefaultTimeZone } = require('./../../constant/DefaultConstant');
import getCurrentLine from 'get-current-line';
import bodyParser = require('body-parser');
import RouteMiddlewareAttendanceService from '../../middleware/RouteMiddlewareAttendanceService';

const attendanceApp = express();

try{

    // REVIEW Set Cors Blocked
      const corsOptions: cors.CorsOptions = {
        origin: '*',
        allowedHeaders: ['api-header-auth', 'content-type', 'authorization'],
      };
  
      attendanceApp.use(cors(corsOptions));
      attendanceApp.use(bodyParser.json());

      const routeMiddleware = express.Router();
      routeMiddleware.use(RouteMiddlewareAttendanceService.mainMiddleware);

    const apiV1Router = express.Router();
    apiV1Router.use('/', routeMiddleware);
    
    const attendance = express.Router();
      attendance.use(RouteMiddlewareAttendanceService.tokenUserMiddleware);
      attendance.post('/', AttendanceController.addAttendance);
      attendance.get('/histories', AttendanceController.getComnbineAttendance);
    apiV1Router.use('/attendance', attendance);
    
    attendanceApp.use('/api/v1', apiV1Router);

} catch(error) {
  LogWinstonHelper.loggerWinstonGlobal.error({
    collectData: {
      date: momentTz
        .tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta)
        .format('YYYY-MM-DD HH:mm:ss'),
      location: getCurrentLine(),
      reqData: [],
    },
    message: error.stack + '\n',
  });
};

export default attendanceApp;