
import moment = require('moment');
import momentTz = require('moment-timezone');
import getCurrentLine from 'get-current-line';
import ApiHelper from '../../helper/ApiHelper';
import { DefaultTimeZone } from '../../constant/DefaultConstant';
import { Attendance } from '../../entity/mysql/attendanceDb/Attendance.entity';
const LogWinstonHelper = require('./../../helper/LogWinstonHelper')

const {attendanceDb} = require('./../../../ormconfig');

class AttendanceService {
    static checkConnectionAttendace = async () : Promise<any> =>{
      try {        
        if(!attendanceDb.isInitialized){
          attendanceDb
              .initialize()
              .then(async() => {
  
              })
              .catch((error) => {
                  LogWinstonHelper.loggerWinstonGlobal.error({
                    collectData: {
                      date: momentTz
                        .tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta)
                        .format('YYYY-MM-DD HH:mm:ss'),
                      location: getCurrentLine(),
                      reqData: [],
                    },
                    message: error.stack + '\n',
                  });
              })
        }
  
        return ApiHelper.buildResponse(true, 'Success Initialized', null)
      } catch (error) {
        LogWinstonHelper.loggerWinstonGlobal.error({
          collectData: {
              date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
              location: getCurrentLine(),
              reqData : null
          },
          message : error.stack + '\n'
        })

        return ApiHelper.builFuncResponse(false, 'Failed Initialized');
      }
    }

    static addAttendance = async (payload) : Promise<any> => {

        const resConnAttendanceDb = await AttendanceService.checkConnectionAttendace()
        if(!resConnAttendanceDb.status){
          return ApiHelper.builFuncResponse(false, resConnAttendanceDb.message);
        }
        if(resConnAttendanceDb.status){
          const queryRunner = attendanceDb.createQueryRunner();
  
          await queryRunner.connect();
          await queryRunner.startTransaction();
  
          try {
  
            // REVIEW Create Add Attendance
              const resAddAttendance = await queryRunner.manager
                  .getRepository(Attendance)
                  .createQueryBuilder()
                  .insert()
                  .values(payload)
                  .execute();

                // REVIEW Get Detail Attendance
                  const collectDetailAttendance = await queryRunner.manager.getRepository(Attendance).findOneBy({
                    id: resAddAttendance.raw.insertId,
                  });
                  
                  await queryRunner.commitTransaction();
                  return ApiHelper.builFuncResponse(true, 'Add PopCenter Success', collectDetailAttendance);
          } catch (err) {
            if (queryRunner.isTransactionActive) await queryRunner.rollbackTransaction();
  
              LogWinstonHelper.loggerWinstonGlobal.error({
                collectData: {
                    date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                    location: getCurrentLine(),
                    reqData : {
                      payload: payload
                    }
                },
                message : err.stack + '\n'
              })
  
              return ApiHelper.builFuncResponse(false, err.message);
          } finally {
            if(!await queryRunner.isReleased){
              await queryRunner.release();
            }
          }
        }

        return ApiHelper.builFuncResponse(false, 'Failed Add Attendance');
    };

    static getFirstAttendance = async ( 
      collectParamFilter:any = {}
    ) : Promise<any> => {

      const resConnAttendanceDb = await AttendanceService.checkConnectionAttendace()
      if(!resConnAttendanceDb.status){
        return ApiHelper.builFuncResponse(false, resConnAttendanceDb.message);
      }

      if(resConnAttendanceDb.status){
        const queryRunner = attendanceDb.createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
          // REVIEW Define Parameter Filter
            const collectSelect = 'collectSelect' in collectParamFilter ? collectParamFilter.collectSelect : []
            const type          = 'type' in collectParamFilter ? collectParamFilter.type : null
            const dateTime      = 'dateTime' in collectParamFilter ? collectParamFilter.dateTime : null
            const uuidEmployee  = 'uuidEmployee' in collectParamFilter ? collectParamFilter.uuidEmployee : null
  
          const queryMain = queryRunner.manager.getRepository(Attendance)
            .createQueryBuilder('Attendance')
  
            if(collectSelect.length > 0){
              queryMain.select(collectSelect);
            }
  
            // REVIEW This Filter
              if(type){
                queryMain.andWhere('Attendance.type = :type', { type: type})
              }
  
              if(dateTime){
                queryMain.andWhere('Attendance.dateTime = :dateTime', { dateTime: dateTime})  
              }

              if(uuidEmployee){
                queryMain.andWhere('Attendance.uuidEmployee = :uuidEmployee', { uuidEmployee: uuidEmployee})  
              }
  
              const resultGetData = await queryMain.getOne()
      
          if (!resultGetData) {
            return ApiHelper.builFuncResponse(false, 'Data Attendance Not Found')
          }
          return ApiHelper.builFuncResponse(true, 'Data Attendance Founded', resultGetData)
    
        } catch (error) {

          if (queryRunner.isTransactionActive) await queryRunner.rollbackTransaction();
  
          LogWinstonHelper.loggerWinstonGlobal.error({
            collectData: {
                date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                location: getCurrentLine(),
                reqData : {
                  collectParamFilter: collectParamFilter,
                }
            },
            message : error.stack + '\n'
          })
  
          return ApiHelper.builFuncResponse(false, 'Something Wrong', error)

        } finally {
          if(!await queryRunner.isReleased){
            await queryRunner.release();
          }
        }
      }

      return ApiHelper.builFuncResponse(false, 'Failed Get Data Attendance');

    }

    static getManyCombineAttendance = async ( 
      collectParamFilter:any = {}
    ) : Promise<any> => {

      const resConnAttendanceDb = await AttendanceService.checkConnectionAttendace()
      if(!resConnAttendanceDb.status){
        return ApiHelper.builFuncResponse(false, resConnAttendanceDb.message);
      }

      if(resConnAttendanceDb.status){
        const queryRunner = attendanceDb.createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
          // REVIEW Define Parameter Filter
            const uuidEmployee = 'uuidEmployee' in collectParamFilter ? collectParamFilter.uuidEmployee : null
            const dateTime     = 'dateTime' in collectParamFilter ? collectParamFilter.dateTime : null

            const funcParseDateUnix = (rangeDateString) => {
              let splitDate = rangeDateString.split(" - ")
              return {
                startAt : splitDate[0],
                endAt   : splitDate[1]
              }
            }  
  
          const queryMain = queryRunner.manager
            .createQueryBuilder()
            .select([
              'uuid_employee AS uuidEmployee',
              'MAX(CASE WHEN type = :clockinType THEN type END) AS clockin_type',
              'date_time AS dateTime',
              'MAX(CASE WHEN type = :clockinType THEN unixtime_date_time END) AS clockin_unixtime',
              'MAX(CASE WHEN type = :clockinType THEN created END) AS clockin_created',
              'MAX(CASE WHEN type = :clockinType THEN updated END) AS clockin_updated',
              'MAX(CASE WHEN type = :clockoutType THEN type END) AS clockout_type',
              'MAX(CASE WHEN type = :clockoutType THEN unixtime_date_time END) AS clockout_unixtime',
              'MAX(CASE WHEN type = :clockoutType THEN created END) AS clockout_created',
              'MAX(CASE WHEN type = :clockoutType THEN updated END) AS clockout_updated',
            ])
            .from(Attendance, 'attendance')
            .where('type = :clockinType OR type = :clockoutType', {
                clockinType: 'CLOCKIN',
                clockoutType: 'CLOCKOUT',
            })
            .groupBy('uuidEmployee, dateTime');


            // REVIEW This Filter
              if (uuidEmployee) {
                  queryMain.having('uuidEmployee = :uuidEmployee', {uuidEmployee: uuidEmployee});
              }

              if(dateTime){
                let parseDateTime = funcParseDateUnix(dateTime)
                queryMain.having('dateTime BETWEEN :startAt AND :endAt', {
                  startAt: parseDateTime.startAt,
                  endAt: parseDateTime.endAt,
                })
              }
        
              const resultGetData = await queryMain.getRawMany();
      
          if (!resultGetData) {
            return ApiHelper.builFuncResponse(false, 'Data Attendance Not Found')
          }
          return ApiHelper.builFuncResponse(true, 'Data Attendance Founded', resultGetData)
    
        } catch (error) {

          if (queryRunner.isTransactionActive) await queryRunner.rollbackTransaction();
  
          LogWinstonHelper.loggerWinstonGlobal.error({
            collectData: {
                date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                location: getCurrentLine(),
                reqData : {
                  collectParamFilter: collectParamFilter,
                }
            },
            message : error.stack + '\n'
          })
  
          return ApiHelper.builFuncResponse(false, 'Something Wrong', error)

        } finally {
          if(!await queryRunner.isReleased){
            await queryRunner.release();
          }
        }
      }

      return ApiHelper.builFuncResponse(false, 'Failed Get Data Attendance');

    }

}

export default AttendanceService;
