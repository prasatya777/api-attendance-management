
import moment = require('moment');
import momentTz = require('moment-timezone');
import getCurrentLine from 'get-current-line';
import ApiHelper from '../../helper/ApiHelper';
import { DefaultTimeZone } from '../../constant/DefaultConstant';
import { Employee } from '../../entity/mysql/attendanceDb/Employee.entity';
const LogWinstonHelper = require('./../../helper/LogWinstonHelper')

const {attendanceDb} = require('./../../../ormconfig');

class EmployeeService {
    static checkConnectionAttendace = async () : Promise<any> =>{
      try {        
        if(!attendanceDb.isInitialized){
          attendanceDb
              .initialize()
              .then(async() => {
  
              })
              .catch((error) => {
                  LogWinstonHelper.loggerWinstonGlobal.error({
                    collectData: {
                      date: momentTz
                        .tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta)
                        .format('YYYY-MM-DD HH:mm:ss'),
                      location: getCurrentLine(),
                      reqData: [],
                    },
                    message: error.stack + '\n',
                  });
              })
        }
  
        return ApiHelper.buildResponse(true, 'Success Initialized', null)
      } catch (error) {
        LogWinstonHelper.loggerWinstonGlobal.error({
          collectData: {
              date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
              location: getCurrentLine(),
              reqData : null
          },
          message : error.stack + '\n'
        })

        return ApiHelper.builFuncResponse(false, 'Failed Initialized');
      }
    }

    static addEmployee = async (payload) : Promise<any> => {

        const resConnAttendanceDb = await EmployeeService.checkConnectionAttendace()
        if(!resConnAttendanceDb.status){
          return ApiHelper.builFuncResponse(false, resConnAttendanceDb.message);
        }
        if(resConnAttendanceDb.status){
          const queryRunner = attendanceDb.createQueryRunner();
  
          await queryRunner.connect();
          await queryRunner.startTransaction();
  
          try {
  
            // REVIEW Create Add Employee
              const resAddEmployee = await queryRunner.manager
                  .getRepository(Employee)
                  .createQueryBuilder()
                  .insert()
                  .values(payload)
                  .execute();

                // REVIEW Get Detail Employee
                  const collectDetailEmployee = await queryRunner.manager.getRepository(Employee).findOneBy({
                    uuidEmployee: payload.uuidEmployee,
                  });
                  
                  await queryRunner.commitTransaction();
                  return ApiHelper.builFuncResponse(true, 'Add PopCenter Success', collectDetailEmployee);
          } catch (err) {
            if (queryRunner.isTransactionActive) await queryRunner.rollbackTransaction();
  
              LogWinstonHelper.loggerWinstonGlobal.error({
                collectData: {
                    date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                    location: getCurrentLine(),
                    reqData : {
                      payload: payload
                    }
                },
                message : err.stack + '\n'
              })
  
              return ApiHelper.builFuncResponse(false, err.message);
          } finally {
            if(!await queryRunner.isReleased){
              await queryRunner.release();
            }
          }
        }

        return ApiHelper.builFuncResponse(false, 'Failed Add Employee');
    };

    static updateEmployee = async (payload, idEmployee) : Promise<any> => {

        const resConnAttendanceDb = await EmployeeService.checkConnectionAttendace()
        if(!resConnAttendanceDb.status){
          return ApiHelper.builFuncResponse(false, resConnAttendanceDb.message);
        }
        if(resConnAttendanceDb.status){
          const queryRunner = attendanceDb.createQueryRunner();
  
          await queryRunner.connect();
          await queryRunner.startTransaction();
  
          try {
  
            // REVIEW Create Update Employee
              const resUpdatePopCenter = await queryRunner.manager
                  .getRepository(Employee)
                  .createQueryBuilder()
                  .update()
                  .set(payload)
                  .where('id = :idEmployee', { idEmployee: idEmployee })
                  .execute();

                // REVIEW Get Detail Employee
                  const collectDetailEmployee = await queryRunner.manager.getRepository(Employee).findOneBy({
                    id: idEmployee,
                  });
                  
                  await queryRunner.commitTransaction();
                  return ApiHelper.builFuncResponse(true, 'Update PopCenter Success', collectDetailEmployee);
          } catch (err) {
            if (queryRunner.isTransactionActive) await queryRunner.rollbackTransaction();
  
              LogWinstonHelper.loggerWinstonGlobal.error({
                collectData: {
                    date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                    location: getCurrentLine(),
                    reqData : {
                      payload: payload
                    }
                },
                message : err.stack + '\n'
              })
  
              return ApiHelper.builFuncResponse(false, err.message);
          } finally {
            if(!await queryRunner.isReleased){
              await queryRunner.release();
            }
          }
        }

        return ApiHelper.builFuncResponse(false, 'Failed Update Employee');
    };

    static getFirstEmployee = async ( 
      collectParamFilter:any = {}
    ) : Promise<any> => {

      const resConnAttendanceDb = await EmployeeService.checkConnectionAttendace()
      if(!resConnAttendanceDb.status){
        return ApiHelper.builFuncResponse(false, resConnAttendanceDb.message);
      }

      if(resConnAttendanceDb.status){
        const queryRunner = attendanceDb.createQueryRunner();

        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
          // REVIEW Define Parameter Filter
            const collectSelect  = 'collectSelect' in collectParamFilter ? collectParamFilter.collectSelect : []
            const email          = 'email' in collectParamFilter ? collectParamFilter.email : null
            const status         = 'status' in collectParamFilter ? collectParamFilter.status : null
            const idEmployee     = 'idEmployee' in collectParamFilter ? collectParamFilter.idEmployee : null
            const uuidEmployee   = 'uuidEmployee' in collectParamFilter ? collectParamFilter.uuidEmployee : null
            const orderByCreated = 'orderByCreated' in collectParamFilter ? collectParamFilter.orderByCreated : false
  
          const queryMain = queryRunner.manager.getRepository(Employee)
            .createQueryBuilder('Employee')
  
            if(collectSelect.length > 0){
              queryMain.select(collectSelect);
            }
  
            // REVIEW This Filter
              if(idEmployee){
                queryMain.andWhere('Employee.id = :idEmployee', { idEmployee: idEmployee})
              }

              if(email){
                queryMain.andWhere('Employee.email = :email', { email: email})
              }
  
              if(status){
                queryMain.andWhere('Employee.status = :status', { status: status})
              }
  
              if(uuidEmployee){
                queryMain.andWhere('Employee.uuidEmployee = :uuidEmployee', { uuidEmployee: uuidEmployee})  
              }
  
              if(orderByCreated){
                queryMain.addOrderBy('Employee.created', 'ASC');
              }
  
              const resultGetData = await queryMain.getOne()
      
          if (!resultGetData) {
            return ApiHelper.builFuncResponse(false, 'Data Employee Not Found')
          }
          return ApiHelper.builFuncResponse(true, 'Data Employee Founded', resultGetData)
    
        } catch (error) {

          if (queryRunner.isTransactionActive) await queryRunner.rollbackTransaction();
  
          LogWinstonHelper.loggerWinstonGlobal.error({
            collectData: {
                date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                location: getCurrentLine(),
                reqData : {
                  collectParamFilter: collectParamFilter,
                }
            },
            message : error.stack + '\n'
          })
  
          return ApiHelper.builFuncResponse(false, 'Something Wrong', error)

        } finally {
          if(!await queryRunner.isReleased){
            await queryRunner.release();
          }
        }
      }

      return ApiHelper.builFuncResponse(false, 'Failed Get Data Employee');

    }

    static getDataTableEmployee = async (
      collectParamFilter:any = {},
    ) : Promise<any> => {

        // REVIEW Define Parameter Filter
          const selectType    = 'selectType' in collectParamFilter ? collectParamFilter.selectType : null
          const collectSelect = 'collectSelect' in collectParamFilter ? collectParamFilter.collectSelect : []
    
        // REVIEW Define Parameter Pagination
          const orderColumn     = 'orderColumn' in collectParamFilter ? collectParamFilter.orderColumn : null
          const typeOrderColumn = 'typeOrderColumn' in collectParamFilter ? collectParamFilter.typeOrderColumn : 'DESC'
          const limitColumn     = 'limitColumn' in collectParamFilter ? collectParamFilter.limitColumn : null
          const offsetColumn    = 'offsetColumn' in collectParamFilter ? collectParamFilter.offsetColumn : null

        const resConnAttendanceDb = await EmployeeService.checkConnectionAttendace()
        if(!resConnAttendanceDb.status){
          return ApiHelper.builFuncResponse(false, resConnAttendanceDb.message);
        }

        if(resConnAttendanceDb.status){
          const queryRunner = attendanceDb.createQueryRunner();
  
          await queryRunner.connect();
          await queryRunner.startTransaction();
  
          try {
  
            const queryMain = queryRunner.manager.getRepository(Employee)
              .createQueryBuilder('employee')

            if(collectSelect.length > 0){
              queryMain.select(collectSelect);
            }
      
            // REVIEW This Filter On Column
      
              if(orderColumn && typeOrderColumn){
                queryMain.addOrderBy(orderColumn, typeOrderColumn)
              }
        
              if(limitColumn){
                queryMain.limit(limitColumn)
              }
        
              if(offsetColumn){
                queryMain.offset(offsetColumn)
              }
        
              let resultData = []
              if(selectType === "count_data"){
                const resultCountData = await queryMain.getRawMany()
                resultData = resultCountData
              }else{
                const resultGetData = await queryMain.getRawMany()
                resultData = resultGetData
              }
      
            if (resultData.length <= 0) {
              return ApiHelper.builFuncResponse(false, 'Data Employee Not Found')
            }
            
            return ApiHelper.builFuncResponse(true, 'Data Employee Founded', resultData)

          } catch (err) {
            if (queryRunner.isTransactionActive) await queryRunner.rollbackTransaction();
  
              LogWinstonHelper.loggerWinstonGlobal.error({
                collectData: {
                    date    : momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                    location: getCurrentLine(),
                    reqData : {
                      collectParamFilter: collectParamFilter
                    }
                },
                message : err.stack + '\n'
              })
  
              return ApiHelper.builFuncResponse(false, err.message);
          } finally {
            if(!await queryRunner.isReleased){
              await queryRunner.release();
            }
          }
        }
  };

}

export default EmployeeService;
