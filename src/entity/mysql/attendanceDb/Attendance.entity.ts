import {
    Entity, 
    PrimaryGeneratedColumn, 
    Column, 
    CreateDateColumn, 
    UpdateDateColumn, 
    DeleteDateColumn,
    Index
} from "typeorm";
import { employeeStatus } from "../../../constant/EmployeeConstant";

@Entity({
    name: 'attendance',
})
export class Attendance {

    @PrimaryGeneratedColumn()
    id: number;

    @Index('attendance-uuidEmployee-idx')
    @Column({
        name  : 'uuid_employee',
        type  : 'uuid',
    })
    uuidEmployee: string;

    @Index('attendance-type-idx')
    @Column({
        name   : 'type'
    })
    type: string;

    @Column({
        name    : 'date_time',
        type    : 'date',
    })
    dateTime?: Date;

    @Column({
        name    : 'unixtime_date_time',
        nullable: true,
        default : 0,
        type    : 'bigint'
    })
    unixtimeDateTime?: number;

    @CreateDateColumn()
    created: Date;
  
    @UpdateDateColumn()
    updated: Date;

    @DeleteDateColumn({
        nullable: true,
    })
    delete?: Date;
}
