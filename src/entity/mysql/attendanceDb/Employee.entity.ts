import {
    Entity, 
    PrimaryGeneratedColumn, 
    Column, 
    CreateDateColumn, 
    UpdateDateColumn, 
    DeleteDateColumn,
    Index
} from "typeorm";
import { employeeStatus } from "../../../constant/EmployeeConstant";

@Entity({
    name: 'employee',
})
export class Employee {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        name  : 'uuid_employee',
        type  : 'uuid',
        unique: true,
    })
    uuidEmployee: string;

    @Column({
        name: 'name',
    })
    name: string;

    @Index('employee-email-idx')
    @Column({
        name: 'email',
    })
    email: string;

    @Column({
        name: 'position_job',
    })
    positionJob: string;

    @Index('employee-phone-idx')
    @Column({
        name    : 'phone',
        nullable: true,
    })
    phone: string;

    @Column({
        name   : 'status',
        default: employeeStatus.INACTIVE,
    })
    status: string;

    @Column({
        name   : 'type'
    })
    type: string;

    @Column({
        name: 'password',
    })
    password: string;

    @Column({
        name    : 'image',
        type    : 'json',
        nullable: true,
    })
    image?: JSON;

    @Column({
        type    : 'datetime',
        name    : 'last_login',
        nullable: true
    })
    lastLogin?: Date;

    @Column({
        name: 'token',
        nullable: true,
        type:'mediumtext'
    })
    token: string;

    @CreateDateColumn()
    created: Date;
  
    @UpdateDateColumn()
    updated: Date;

    @DeleteDateColumn({
        nullable: true,
    })
    delete?: Date;
}
