import moment   = require('moment');
import momentTz = require('moment-timezone');
import fs       = require('fs');

const {attendanceDb} = require('./../ormconfig');

import getCurrentLine from 'get-current-line';
const LogWinstonHelper    = require('./../src/helper/LogWinstonHelper');
import { DefaultTimeZone } from './constant/DefaultConstant';
import { pathConstantLog, pathConstantImage } from './constant/PathConstant';

(async() => {

  // REVIEW Create Folder For Logger
    const dirPathLog = [
      { path: './src/log/', chmod: 0o755 },
      { path: './src/log/cron', chmod: 0o755 },
      { path: './src/storage/', chmod: 0o755 },
      { path: './src/storage/image/', chmod: 0o755 },
      { path: pathConstantLog.error, chmod: 0o755 },
      { path: pathConstantLog.http, chmod: 0o755 },
      { path: pathConstantLog.info, chmod: 0o755 },
      { path: pathConstantLog.exception, chmod: 0o755 },
      { path: pathConstantLog.errorCron, chmod: 0o755 },
      { path: pathConstantLog.exceptionCron, chmod: 0o755 },
      { path: pathConstantLog.infoCron, chmod: 0o755 },

      { path: pathConstantImage.employee, chmod: 0o755 },

    ];

    await Promise.all(
      dirPathLog.map(async (v) => {
        if (!fs.existsSync(v.path)) {
          await fs.mkdirSync(v.path);
          await fs.chmod(v.path, v.chmod, async () => {
            console.log(v.path + ' Change Access to : ' + v.chmod.toString());
          });
        } else {
          await fs.chmod(v.path, v.chmod, () => {
            console.log(v.path + ' Change Access to : ' + v.chmod.toString());
          });
        }
      }),
    );

    if(!attendanceDb.isInitialized){
      attendanceDb
          .initialize()
          .then(async() => {
            LogWinstonHelper.loggerCron.info({
              collectData: {
                date: momentTz.tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta).format('YYYY-MM-DD HH:mm:ss'),
                task: 'attendanceDb-initialized',
                type: 'start-listening',
              },
              message: `Data Source MySql attendanceDb has been initialized`
            });
          }).catch((error) => {
              LogWinstonHelper.loggerWinstonGlobal.error({
                collectData: {
                  date: momentTz
                    .tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta)
                    .format('YYYY-MM-DD HH:mm:ss'),
                  location: getCurrentLine(),
                  reqData: [],
                },
                message: error.stack + '\n',
              });
          })
    }

    // REVIEW Run Cron Script
      require('./../src/helper/api/MainApiConfig');

})().then( (e) => {
  
}).catch((error) => {
    LogWinstonHelper.loggerWinstonGlobal.error({
      collectData: {
        date: momentTz
          .tz(Number(moment().format('x')), DefaultTimeZone.asiaJakarta)
          .format('YYYY-MM-DD HH:mm:ss'),
        location: getCurrentLine(),
        reqData: [],
      },
      message: error.stack + '\n',
    });
});