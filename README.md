# Express Js API with TypeORM

## First Action After `Clone Repository`

1. Copy `.env.example` to `.env` and fill `parameters` on file `.env`
2. Please make sure already installed node version `v 19.7.0`
3. Install package `npm` used `command on terminal` : `npm i` or `npm install`
4. Run migration table used `command on terminal` : `npm run typeorm-attendanceDb:run`
5. Last run used `command on terminal` : `npm run start` or `npm run dev`